import configparser
import random 
import json 
import requests
from flask import Flask, render_template
from flask import request,redirect,Response
from flask import jsonify

config = configparser.ConfigParser()
config.read("config.ini")
app = Flask(__name__)


@app.route('/add_friend/',methods=["POST"])
def add_friend():
    """
    Given a IP, it will query the given IP and add as a friend in friends.txt

    :methods POST 
    :params

    ip : IP address 

    """

    auth_key = company = request.headers.get('X-Admin-Auth-Token')
    request_data = json.loads(request.get_data())
    friend_ip = request_data["ip"]
    friend_name = request_data["name"]
    friend_details = friend_ip+" "+friend_name.replace(" ","_")    
    friend_url = "http://"+request_data["ip"]+":"+config["friends"]["port"] + config["friends"]["auth_url"]
    try:
        friend_validate = requests.get(friend_url)
    except Exception as e:
        return Response("Friend not responding. Is friend server up or IP current ?")
    if friend_validate.status_code == 200:
        friend_response = friend_validate.text.strip()
        if friend_response == config["friends"]["secret"]:
                with open("friends.txt","r") as friends_file_read:
                    current_friends = friends_file_read.read()
                    if request_data["ip"] not in current_friends:
                        with open("friends.txt","a+") as friends_file_write:
                            friends_file_write.write(friend_details+"\n")
                        return Response("Friend added. Other friends will be updated soon")
                    else:
                        return Response("Friend already known. Not doing anything")
        else:
            return Response("Friend validation failed. Not adding as friend")
    else:
        return Response("Key error. Key file not found for authentication of a friend")

@app.route("/get_recorded_count/",methods=["GET"])
def get_recorded_count():
    if config["server"]["admin"]:
        auth_key = company = request.headers.get('X-Auth-Token')
        friend_count = {}
        with open("friends.txt","r") as friends_file:
            friends_list = friends_file.readlines()
            for friend in friends_list:
                friend_ip =  friend.split(" ")[0]
                recorded_data_url = "http://"+friend_ip+":"+config["friends"]["port"] + config["friends"]["recorded_url_meta"]
                try:
                    count_request = requests.get(recorded_data_url)
                    if count_request.status_code == 200:
                        data = count_request.text.split("\n")
                        friend_count[friend_ip] = {"count":data[0].split(",")[1],"disk_usage": data[1].split(",")[1]}
                except Exception:
                    friend_count[friend_ip] = {"error":"Server Down"}
        return render_template("admin_recorded_count.html",friend_count=friend_count)
    else:
        return Response("Not a admin user, access denied")
    

@app.route("/",methods=["GET"])
def index():
    return render_template("index.html")

@app.route("/audio/category/<int:cat_id>",methods=["GET"])
def listen_audio(cat_id):
    if (cat_id == 999):
        try:
            rand_cat_id = random.randint(1,2)
            friend_public_url = "http://127.0.0.1:"+config["friends"]["port"] + config["friends"]["public_url"]+str(rand_cat_id)
            audios = requests.get(friend_public_url+"/list_of_audio_files")
            if audios.status_code == 200:
                list_of_audios_uncleaned = audios.text.replace("\n",",").split(",")
                list_of_audios = list_of_audios_uncleaned.pop(-1)
                return jsonify({"audios":list_of_audios_uncleaned,"friend_public_url":friend_public_url}),200
        except Exception:
            return jsonify({"error":"Unable to query friends"})
    else:
        with open("friends.txt","r") as friends_file:
            friends_list = friends_file.readlines()
            friend =  random.choice(friends_list)
            friend_ip = friend.split(" ")[0]
            friend_public_url = "http://"+friend_ip+":"+config["friends"]["port"] + config["friends"]["public_url"]+str(cat_id)
            try:
                audios = requests.get(friend_public_url+"/list_of_audio_files")
                if audios.status_code == 200:
                    list_of_audios_uncleaned = audios.text.replace("\n",",").split(",")
                    list_of_audios = list_of_audios_uncleaned.pop(-1)
                    return jsonify({"audios":list_of_audios_uncleaned,"friend_public_url":friend_public_url}),200
            except Exception:
                return jsonify({"error":"Unable to query friends"})


if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0")

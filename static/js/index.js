var audios = NaN
var friend_url = NaN
var audio_current = 0
var audio_elem = document.getElementById("audioplayer")
var play_button = document.getElementById("play")

if (localStorage.getItem("favs") === null) {
    localStorage.setItem("favs",JSON.stringify({"audios":[]}))
  }
function loadAudio(id) {
    var xhr = new XMLHttpRequest(); 
	xhr.onload = function() {
        // Process our return data
        if (xhr.status >= 200 && xhr.status < 300) {
            audio_response = JSON.parse(xhr.response)
            friend_url = audio_response["friend_public_url"]
            audios = []
            for (i=0;i<audio_response["audios"].length;i++){
                audios.push(friend_url + "/" +audio_response["audios"][i])                 
            }
            audio_current = 0
            audio_elem.src = audios[audio_current]
            document.getElementById("play").innerText = "||"
            audio_elem.play()
            
        } else {
            alert("Something failed. Try agian.")
        }      
    };
    xhr.open('GET', '/audio/category/'+id);
    xhr.send();
};

function change_music(action){
    if (friend_url) {
        var audio_id = NaN
        if (action == "next"){
            audio_id = audio_current + 1 
        } else {
            audio_id = audio_current -1 
        }
        if(audio_id < audios.length && audio_id >= 0 ){
            audio_current = audio_id
        } else {
            audio_current = 0
        }
        audio_elem.src = audios[audio_current]
        play_button.innerText = "||";
    }

}

play_button.onclick = function(){
    if (play_button.innerText == "||"){
        audio_elem.pause()
        play_button.innerHTML= '<img src="/static/icons/play-circle-solid.svg"/>';
    } else {
        play_button.innerText = "||";
        audio_elem.play()
    }
}

function make_fav(){
    cur_favs = JSON.parse(localStorage.getItem("favs"))
    var audio_fav = audios[audio_current]
    if (cur_favs["audios"].includes(audio_fav )){
        console.log("Already in fav. Not adding")
    } else {
        cur_favs["audios"].push(audio_fav)
        localStorage.setItem("favs",JSON.stringify(cur_favs))
    }
    console.log(localStorage.getItem("favs"))

}

function play_fav(){
    audios = NaN
    audios = JSON.parse(localStorage.getItem("favs"))["audios"]
    audio_current = 0
    audio_elem.src = audios[audio_current]
    document.getElementById("play").innerText = "||"
    audio_elem.play()

}

audio_elem.onended = function() {
  change_music("next") 
};
